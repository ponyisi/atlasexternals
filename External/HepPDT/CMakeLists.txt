# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building HepPDT for ATLAS.
#

# The package's name:
atlas_subdir( HepPDT )

# The source code for HepPDT:
set( ATLAS_HEPPDT_SOURCE
   "http://cern.ch/lcgpackages/tarFiles/sources/HepPDT-2.06.01.tar.gz"
   CACHE STRING "HepPDT source file to use" )
set( ATLAS_HEPPDT_HASH "5688b4bdbd84b48ed5dd2545a3dc33c0"
   CACHE STRING "MD5 hash for the HepPDT source file" )
mark_as_advanced( ATLAS_HEPPDT_SOURCE ATLAS_HEPPDT_HASH )

# Directory for the temporary build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/heppdtBuild" )

# Figure out the build platform to specify for HepPDT. This is necessary because
# the HepPDT source code can not recognise some of the platforms that we now
# use. (Most notably aarch64.) Even though the HepPDT build doesn't seem to use
# this platform name in any useful way, it still fails if it can't figure out
# what platform it's running on. :-/
string( TOLOWER
   "${CMAKE_HOST_SYSTEM_PROCESSOR}-unknown-${CMAKE_HOST_SYSTEM_NAME}"
   _buildArch )

# Set up the build of HepPDT:
ExternalProject_Add( HepPDT
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   URL "${ATLAS_HEPPDT_SOURCE}"
   URL_MD5 "${ATLAS_HEPPDT_HASH}"
   CONFIGURE_COMMAND <SOURCE_DIR>/configure --prefix=${_buildDir}
   --build=${_buildArch}
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>" )
ExternalProject_Add_Step( HepPDT purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for HepPDT"
   DEPENDEES download
   DEPENDERS patch )
add_dependencies( Package_HepPDT HepPDT )

# Install HepPDT:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
