# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Package building CORAL as part of the offline build procedure.
#

# The name of the package.
atlas_subdir( CORAL )

# Check whether CORAL is to be built in the current project.
if( NOT ATLAS_BUILD_CORAL )
   return()
endif()

# Let the user know what's happening.
message( STATUS "Building CORAL as part of this project" )

# Source of the CORAL code to build.
set( ATLAS_CORAL_REPOSITORY "https://gitlab.cern.ch/lcgcoral/coral.git"
   CACHE STRING "Repository to fetch CORAL from" )
set( ATLAS_CORAL_TAG "CORAL_3_3_10"
   CACHE STRING "Git tag to use for the CORAL build" )
mark_as_advanced( ATLAS_CORAL_REPOSITORY ATLAS_CORAL_TAG )

# Arguments to give to the CMake configuration of CORAL.
set( _cmakeArgs )

# Set up where to get Python from.
if( ATLAS_BUILD_PYTHON )
   list( APPEND _cmakeArgs
      -DPYTHON_EXECUTABLE:FILEPATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_BINDIR}/python${CMAKE_EXECUTABLE_SUFFIX}
      -DPYTHON_LIBRARY:FILEPATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}Python${CMAKE_SHARED_LIBRARY_SUFFIX}
      -DPYTHON_INCLUDE_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_INCLUDEDIR}
      -DPython_config_version_twodigit:STRING=3.9
      -DLCG_python3:STRING=on )
else()
   find_package( Python COMPONENTS Interpreter Development )
   if( Python_FOUND )
      list( APPEND _cmakeArgs
         -DPYTHON_EXECUTABLE:FILEPATH=${Python_EXECUTABLE}
         -DPYTHON_LIBRARY:FILEPATH=${Python_LIBRARIES}
         -DPYTHON_INCLUDE_DIR:PATH=${Python_INCLUDE_DIRS}
         -DPython_config_version_twodigit:STRING=${Python_VERSION_MAJOR}.${Python_VERSION_MINOR} )
      if( ${Python_VERSION} VERSION_GREATER_EQUAL 3 )
         list( APPEND _cmakeArgs -DLCG_python3:STRING=on )
      endif()
   endif()
endif()

# Set up whete to get Boost from.
if( ATLAS_BUILD_BOOST )
   list( APPEND _cmakeArgs
      -DBOOST_ROOT:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} )
else()
   find_package( Boost CONFIG )
   if( Boost_FOUND )
      list( APPEND _cmakeArgs
         -DBOOST_ROOT:PATH=${Boost_DIR} )
   endif()
endif()

# Set up where to get CppUnit from.
find_package( CppUnit )
if( CPPUNIT_FOUND )
   list( APPEND _cmakeArgs
      -DCPPUNIT_INCLUDE_DIR:PATH=${CPPUNIT_INCLUDE_DIR}
      -DCPPUNIT_LIBRARY:FILEPATH=${CPPUNIT_cppunit_LIBRARY} )
endif()

# Set up where to get XercesC from.
if( ATLAS_BUILD_XERCESC )
   list( APPEND _cmakeArgs
      -DXERCESC_INCLUDE_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_INCLUDEDIR}
      -DXERCESC_LIBRARY:FILEPATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}xerces-c${CMAKE_SHARED_LIBRARY_SUFFIX}
      -DXERCESC_EXECUTABLE:FILEPATH=DummyNotNeeded )
else()
   find_package( XercesC )
   if( XERCESC_FOUND )
      list( APPEND _cmakeArgs
         -DXERCESC_INCLUDE_DIR:PATH=${XercesC_INCLUDE_DIR}
         -DXERCESC_LIBRARY:FILEPATH=${XercesC_LIBRARY}
         -DXERCESC_EXECUTABLE:FILEPATH=DummyNotNeeded )
   endif()
endif()

# Set up where to take SQLite from.
find_package( SQLite3 )
if( SQLITE3_FOUND )
   list( APPEND _cmakeArgs
      -DSQLITE_INCLUDE_DIR:PATH=${SQLite3_INCLUDE_DIR}
      -DSQLITE_LIBRARY:FILEPATH=${SQLite3_LIBRARY}
      -DSQLITE_EXECUTABLE:FILEPATH=DummyNotNeeded )
endif()

# Set up where to take MySQL from.
find_package( mysql )
if( MYSQL_FOUND )
   list( APPEND _cmakeArgs
      -DMYSQL_INCLUDE_DIR:PATH=${MYSQL_INCLUDE_DIR}
      -DMYSQL_LIBRARY:FILEPATH=${MYSQL_mysqlclient_LIBRARY}
      -DMYSQL_EXECUTABLE:FILEPATH=${MYSQL_mysql_EXECUTABLE} )
endif()

# Set up where to take libaio from.
find_package( libaio )
if( LIBAIO_FOUND )
   list( APPEND _cmakeArgs
      -DLIBAIO_LIBRARIES:FILEPATH=${LIBAIO_aio_LIBRARY} )
endif()

# Set up where to take Oracle from.
find_package( Oracle )
if( ORACLE_FOUND )
   list( APPEND _cmakeArgs
      -DORACLE_INCLUDE_DIR:PATH=${ORACLE_INCLUDE_DIR}
      -DORACLE_LIBRARY:FILEPATH=${ORACLE_clntsh_LIBRARY}
      -DSQLPLUS_EXECUTABLE:FILEPATH=${SQLPLUS_EXECUTABLE} )
endif()

# Set up where to take EXPAT from.
find_package( EXPAT )
if( EXPAT_FOUND )
   list( APPEND _cmakeArgs
      -DEXPAT_INCLUDE_DIR:PATH=${EXPAT_INCLUDE_DIR}
      -DEXPAT_LIBRARY:FILEPATH=${EXPAT_LIBRARY}
      -DEXPAT_EXECUTABLE:FILEPATH=DummyNotNeeded )
endif()

# Set up where to take Frontier from.
find_package( Frontier_Client )
if( FRONTIER_CLIENT_FOUND )
   list( APPEND _cmakeArgs
      -DFRONTIER_CLIENT_INCLUDE_DIR:PATH=${FRONTIER_CLIENT_INCLUDE_DIR}
      -DFRONTIER_CLIENT_LIBRARY:FILEPATH=${FRONTIER_CLIENT_frontier_client_LIBRARY}
      -DFRONTIER_CLIENT_EXECUTABLE:FILEPATH=DummyNotNeeded )
endif()

# Set up where to take Valgrind from.
find_package( valgrind )
if( VALGRIND_FOUND )
   list( APPEND _cmakeArgs
      -DVALGRIND_EXECUTABLE:FILEPATH=${VALGRIND_valgrind_EXECUTABLE} )
endif()

# Set up where to take libunwind from.
find_package( libunwind )
if( LIBUNWIND_FOUND )
   list( APPEND _cmakeArgs
      -DUNWIND_INCLUDE_DIR:PATH=${LIBUNWIND_INCLUDE_DIR}
      -DUNWIND_LIBRARIES:FILEPATH=${LIBUNWIND_unwind_LIBRARY} )
endif()

# Set up where to take IgProf from.
find_package( igprof )
if( IGPROF_FOUND )
   list( APPEND _cmakeArgs
      -DIGPROF_LIBRARY:FILEPATH=${IGPROF_igprof_LIBRARY}
      -DIGPROF_EXECUTABLE:FILEPATH=${IGPROF_igprof_EXECUTABLE} )
endif()

# Set up where to take GPerfTools from.
find_package( gperftools COMPONENTS tcmalloc profiler )
if( GPERFTOOLS_FOUND )
   list( APPEND _cmakeArgs
      -DGPERFTOOLS_INCLUDE_DIR:PATH=${GPERFTOOLS_INCLUDE_DIR}
      -DGPERFTOOLS_tcmalloc_LIBRARY:FILEPATH=${GPERFTOOLS_tcmalloc_LIBRARY}
      -DGPERFTOOLS_profiler_LIBRARY:FILEPATH=${GPERFTOOLS_profiler_LIBRARY}
      -DPPROF_EXECUTABLE:FILEPATH=${GPERFTOOLS_pprof_EXECUTABLE} )
endif()

# Extra options for the configuration.
if( NOT "${CMAKE_CXX_STANDARD}" EQUAL "" )
   list( APPEND _cmakeArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _cmakeArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Directory for the temporary build results.
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CORALBuild" )
# Directory holding the "stamp" files.
set( _stampDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CORALStamp" )

# Build CORAL for the build area.
ExternalProject_Add( CORAL
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   STAMP_DIR "${_stampDir}"
   GIT_REPOSITORY "${ATLAS_CORAL_REPOSITORY}"
   GIT_TAG "${ATLAS_CORAL_TAG}"
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DBINARY_TAG:STRING=${ATLAS_PLATFORM}
   ${_cmakeArgs}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( CORAL cleansource
   COMMAND ${CMAKE_COMMAND} -E remove -f
   "${_stampDir}/CORAL-gitclone-lastrun.txt"
   DEPENDERS download )
ExternalProject_Add_Step( CORAL forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "Forcing the re-download of CORAL (2021.07.29.)"
   DEPENDERS cleansource )
ExternalProject_Add_Step( CORAL purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for CORAL"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( CORAL buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove -f "${_buildDir}/cc-run"
      "${_buildDir}/cc-sh" "${_buildDir}/qmtestRun.sh"
      "${_buildDir}/run_nightly_tests_cmake.sh"
   COMMAND ${CMAKE_COMMAND} -E remove_directory -f "${_buildDir}/CoralTest"
      "${_buildDir}/env" "${_buildDir}/tests"
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing CORAL into the build area"
   DEPENDEES install )
add_dependencies( Package_CORAL CORAL )
if( ATLAS_BUILD_PYTHON )
   add_dependencies( CORAL Python )
endif()
if( ATLAS_BUILD_BOOST )
   add_dependencies( CORAL Boost )
endif()
if( ATLAS_BUILD_XERCESC )
   add_dependencies( CORAL XercesC )
endif()

# Install CORAL.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
