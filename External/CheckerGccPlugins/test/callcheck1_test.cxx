// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
// Test callcheck for event Handle classes.


namespace std
{
template <typename _Tp> class unique_ptr {};
template<typename _Tp>
struct _MakeUniq { typedef unique_ptr<_Tp> __single_object; };
template <class T> class vector {};
}

namespace Gaudi {
class DataHandle
{
public:
  enum Mode { Reader = 1 << 2, Writer = 1 << 4, Updater = Reader | Writer };
};
}


class EventContext {};
template <class DV> class ConstDataVector {};


namespace SG {


template <class T> class DataObjectSharedPtr {};


//*********************************************************************************


template <class T>  class ReadHandleKey {};


template <class T>
class ReadHandle
{ 
public: 
  ReadHandle();
  explicit ReadHandle (const ReadHandleKey<T>& key);
  explicit ReadHandle (const ReadHandleKey<T>& key, const EventContext& ctx);
  typedef const T*   const_pointer_type;
  const_pointer_type get() const;
  const_pointer_type get (const EventContext& ctx) const;
}; 


template <class T>
ReadHandle<T> makeHandle (const ReadHandleKey<T>& key);
template <class T>
ReadHandle<T> makeHandle (const ReadHandleKey<T>& key,
                          const EventContext& ctx);


//*********************************************************************************


template <class T>  class WriteHandleKey {};

template <class T>
class WriteHandle
{ 
public: 
  WriteHandle();
  explicit WriteHandle (const WriteHandleKey<T>& key);
  explicit WriteHandle (const WriteHandleKey<T>& key, const EventContext& ctx);
  typedef const T*   const_pointer_type;

  const_pointer_type put (std::unique_ptr<T> data,
                          bool returnExisting = false) const;
  const_pointer_type put (const EventContext& ctx,
                          std::unique_ptr<T> data,
                          bool returnExisting = false) const;

  const_pointer_type put (std::unique_ptr<const T> data,
                          bool returnExisting = false) const;
  const_pointer_type put (const EventContext& ctx,
                          std::unique_ptr<const T> data,
                          bool returnExisting = false) const;

  const_pointer_type put (std::unique_ptr<const ConstDataVector<T> > data,
                          bool returnExisting = false) const;
  const_pointer_type put (const EventContext& ctx,
                          std::unique_ptr<const ConstDataVector<T> > data,
                          bool returnExisting = false) const;

  const_pointer_type put (SG::DataObjectSharedPtr<T> data) const;
  const_pointer_type put (const EventContext& ctx,
                          SG::DataObjectSharedPtr<T> data) const;

  template <class AUXSTORE>
  const_pointer_type
  put (std::unique_ptr<T> data,
       std::unique_ptr<AUXSTORE> auxstore) const;
  template <class AUXSTORE>
  const_pointer_type
  put (const EventContext& ctx,
       std::unique_ptr<T> data,
       std::unique_ptr<AUXSTORE> auxstore) const;

  template <class AUXSTORE>
  const_pointer_type
  put (std::unique_ptr<const T> data,
       std::unique_ptr<const AUXSTORE> auxstore) const;
  template <class AUXSTORE>
  const_pointer_type
  put (const EventContext& ctx,
       std::unique_ptr<const T> data,
       std::unique_ptr<const AUXSTORE> auxstore) const;
}; 

template <class T>
WriteHandle<T> makeHandle (const WriteHandleKey<T>& key);
template <class T>
WriteHandle<T> makeHandle (const WriteHandleKey<T>& key,
                           const EventContext& ctx);


//*********************************************************************************


template <class T>  class UpdateHandleKey {};


template <class T>
class UpdateHandle
{ 
public: 
  UpdateHandle();
  explicit UpdateHandle (const UpdateHandleKey<T>& key);
  explicit UpdateHandle (const UpdateHandleKey<T>& key, const EventContext& ctx);
}; 

template <class T>
UpdateHandle<T> makeHandle (const UpdateHandleKey<T>& key);
template <class T>
UpdateHandle<T> makeHandle (const UpdateHandleKey<T>& key,
                            const EventContext& ctx);


//*************************************************************************

template <class T>  class ReadDecorHandleKey {};


template <class T, class D>
class ReadDecorHandle
{
public:
  ReadDecorHandle(const ReadDecorHandleKey<T>& key);
  ReadDecorHandle(const ReadDecorHandleKey<T>& key, const EventContext& ctx);
};


template <class T>
void makeHandle (const ReadDecorHandleKey<T>& key);
template <class T>
void makeHandle (const ReadDecorHandleKey<T>& key,
                 const EventContext& ctx);


//*************************************************************************

template <class T>  class WriteDecorHandleKey {};


template <class T, class D>
class WriteDecorHandle
{
public:
  WriteDecorHandle(const WriteDecorHandleKey<T>& key);
  WriteDecorHandle(const WriteDecorHandleKey<T>& key, const EventContext& ctx);
};


template <class T>
void makeHandle (const WriteDecorHandleKey<T>& key);
template <class T>
void makeHandle (const WriteDecorHandleKey<T>& key,
                 const EventContext& ctx);


//*********************************************************************************


template <class T_Handle, class T_HandleKey, Gaudi::DataHandle::Mode MODE>
class HandleKeyArray
{
public:
  std::vector< T_Handle > makeHandles() const;
  std::vector< T_Handle > makeHandles (const EventContext& ctx) const;
};


}


std::unique_ptr<int> getptr();
std::unique_ptr<const int> getptr_c();
std::unique_ptr<const ConstDataVector<int> > getptr_dv();
SG::DataObjectSharedPtr<int> getptr_sp();
std::unique_ptr<double> getptr_double();
std::unique_ptr<const double> getptr_double_c();


void testReadHandle (const SG::ReadHandleKey<int>& k, const EventContext& xyzzy)
{
  SG::ReadHandle<int> h1 (k, xyzzy);
  SG::ReadHandle<int> h2 (k);
  SG::makeHandle (k);
  //h2.get();
}


void testWriteHandle (const SG::WriteHandleKey<int>& k,
                      const EventContext& xyzzy)
{
  SG::WriteHandle<int> h2 (k, xyzzy);
  SG::WriteHandle<int> h (k);
  SG::makeHandle (k);
  //h.put (getptr());
  //h.put (getptr_c());
  //h.put (getptr_dv());
  //h.put (getptr_sp());
  //h.put (getptr(), getptr_double());
  //h.put (getptr_c(), getptr_double_c());
}


void testUpdateHandle (const SG::UpdateHandleKey<int>& k, const EventContext& xyzzy)
{
  SG::UpdateHandle<int> h2 (k, xyzzy);
  SG::UpdateHandle<int> h (k);
  SG::makeHandle (k);
}


void testReadDecorHandle (const SG::ReadDecorHandleKey<int>& k,
                          const EventContext* xyzzy)
{
  SG::ReadDecorHandle<int, float> h2 (k, *xyzzy);
  SG::ReadDecorHandle<int, float> h (k);
  SG::makeHandle (k);
}


void testWriteDecorHandle (const SG::WriteDecorHandleKey<int>& k,
                           const EventContext&)
{
  SG::WriteDecorHandle<int, float> h (k);
  SG::makeHandle (k);
}


void testHandleKeyArray (const SG::HandleKeyArray<int, float, Gaudi::DataHandle::Reader>& k,
                         const EventContext& xyzzy)
{
  k.makeHandles();
  k.makeHandles(xyzzy);
}


void testNoContext (const SG::WriteDecorHandleKey<int>& k)
{
  SG::WriteDecorHandle<int, float> h (k);
}



