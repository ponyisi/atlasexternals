#!/usr/bin/env bash
#
# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Wrapper for flake8 with some ATLAS-specific options and default settings
# to be used for build integration or implement flake8-based unit tests.
#
# Additional features compared to flake8:
#   - Check only files with content matching pattern (--filterFiles).
#   - Steer number of parallel jobs via environment variables.
#   - Return always with 0 exit code, except for unit tests.
#

# Default arguments for flake8 that cannot be overwritten by user:
default_args="--show-source --format='%(path)s:%(row)d:%(col)d: warning: %(code)s %(text)s'"

function usage() {
    flake8 --help
    cat <<EOF

ATLAS specific options:
Default: ${default_args}

  --filterFiles=REGEX      Only check files with at least one line matching REGEX.
  --extend-extensions=EXT  Comma-separated list of extensions to add to enabled ones.

  \$FLAKE8_ATLAS_JOBS       Environment variable to force number of parallel jobs.
                           (overwrites command line arguments -j/--jobs)
  \$FLAKE8_ATLAS_VERBOSE    Enable verbose output (e.g. print flake8 command)

EOF
}

args=""
njobs="auto"
enable_extensions=""
extend_extensions=""

# Hack: Normalize long options to use space as separator
set -- $(echo "$@" | sed -r 's#(--[[:alnum:]_-]+)=#\1 #g')

while [[ $# -gt 0 ]]; do
    case $1 in
        # Parse flake8 options:
        -j|--jobs)
            shift
            njobs=$1
            shift
            ;;
        --enable-extensions)
            shift
            enable_extensions=$1
            shift
            ;;
        # Additional options:
        --filterFiles)
            shift
            pattern=$1
            shift
            ;;
        --extend-extensions)
            shift
            extend_extensions=$1
            shift
            ;;
        # Other options:
        -h|--help)
            usage
            exit 0
            ;;
        --)
            shift
            files="$@"
            break
            ;;
        # Otherwise just pass them on to flake8:
        *)
            args+=" $1"
            shift
            ;;
    esac
done

# Handle --enable/extend-extensions:
if [ -n "$extend_extensions" ]; then
    enable_extensions=${enable_extensions:+${enable_extensions},}${extend_extensions}  # append
fi
if [ -n "$enable_extensions" ]; then
    args="--enable-extensions ${enable_extensions} ${args}";
fi

# Force number of parallel jobs:
if [ -n "${FLAKE8_ATLAS_JOBS}" ]; then
    njobs=${FLAKE8_ATLAS_JOBS}
fi

# Filter files containing pattern:
if [ ! -z "${pattern}" ]; then
    if [ -z "${files}" ]; then
        echo "warning: No input files given to flake8_atlas. The use of --filterFiles requires file names to be passed after '--'."
        test -z "${ATLAS_CTEST_PACKAGE}"
        exit $?   # failure(1) in unit test otherwise continue(0)
    fi
    files=`grep -Erl "${pattern}" ${files}`
    if [ -z "${files}" ]; then
        exit 0
    fi
fi

if [ -n "${files}" ]; then
    args+=" -- ${files}"
fi

# Run flake8:
if ! type flake8 > /dev/null 2>&1; then
    # Print warning that will be picked up by nightly/CI patterns (ATEAM-797)
    echo "flake8_atlas: warning: flake8 executable not found."
    rc=1
else
    cmd="flake8 --jobs ${njobs} ${default_args} ${args}"
    if [ -n "${FLAKE8_ATLAS_VERBOSE}" ]; then
        echo "Executing: $cmd"
    fi
    eval $cmd
    rc=$?

    if [ $rc -ne 0 ]; then
        echo "warning: Python code violations found. See <https://gitlab.cern.ch/atlas/atlasexternals/tree/master/External/flake8_atlas>."
    fi
fi

# In unit test return with flake8 code, otherwise 0:
if [ -n "${ATLAS_CTEST_PACKAGE}" ]; then
    exit $rc
else
    exit 0
fi
