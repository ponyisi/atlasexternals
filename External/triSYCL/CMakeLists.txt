# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building triSYCL as part of the offline software build.
#

# Set the package name.
atlas_subdir( triSYCL )

# Stop if the build is not needed.
if( NOT ATLAS_BUILD_TRISYCL )
   return()
endif()

# Tell the user what's happening.
message( STATUS "Building triSYCL as part of this project" )

# Find Boost, as it's needed by triSYCL.
find_package( Boost )
find_package( Rangev3 )

# The triSYCL source.
set( ATLAS_TRISYCL_SOURCE
   "http://cern.ch/atlas-software-dist-eos/externals/triSYCL/triSYCL-fbfc9c4d.tar.bz2"
   CACHE STRING "triSYCL source file to use" )
set( ATLAS_TRISYCL_HASH "32ff10134b962c2039aa7859125276af"
   CACHE STRING "MD5 hash for the triSYCL source file" )
mark_as_advanced( ATLAS_TRISYCL_SOURCE ATLAS_TRISYCL_HASH )

# Generate our own range-v3-config.cmake file, since the one provided by LCG is
# not functional. :-(
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/range-v3-config.cmake.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/range-v3-config.cmake"
   @ONLY )

# The temporary directory to set up the built results in.
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/triSYCLBuild" )

# Set up the build of triSYCL for the build area.
ExternalProject_Add( triSYCL
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   URL "${ATLAS_TRISYCL_SOURCE}"
   URL_MD5 "${ATLAS_TRISYCL_HASH}"
   CMAKE_CACHE_ARGS -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
   -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_INCLUDEDIR:PATH=${CMAKE_INSTALL_INCLUDEDIR}/triSYCL
   -DBOOST_INCLUDEDIR:PATH=${Boost_INCLUDE_DIRS}
   -DBOOST_LIBRARYDIR:PATH=${Boost_LIBRARY_DIRS}
   -Drange-v3_DIR:PATH=${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}
   -DBUILD_TESTING:BOOL=FALSE
   -DTRISYCL_OPENMP:BOOL=FALSE -DTRISYCL_TBB:BOOL=FALSE
   -DTRISYCL_OPENCL:BOOL=FALSE
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( triSYCL forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo
   "Forcing the re-download of triSYCL (2021.04.26.)"
   DEPENDERS download )
ExternalProject_Add_Step( triSYCL buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}/share"
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing triSYCL into the build area"
   DEPENDEES install )
add_dependencies( Package_triSYCL triSYCL )

# Install triSYCL.
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
