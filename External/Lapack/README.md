LAPACK — Linear Algebra PACKage
===============================

This package builds LAPACK for the offline software of ATLAS.

The sources are downloaded directly from http://www.netlib.org/lapack.
