# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building Geant4 with all its ATLAS specific patches, for the offline
# builds.
#

# The name of the package:
atlas_subdir( Geant4 )

# The externals needed for the build. Note that we could actually
# skip finding the packages here. We just look for them already here
# to ease debugging in case they can't be found later on by Geant4 itself.
# And to set up a proper RPM dependency on them...
find_package( XercesC )
find_package( EXPAT )

# Git repository for Geant4:
# NB: When updating the Geant4 major/minor version, you should check the
# release notes for any updates to the names/environment variables of the
# data libraries utilized by the toolkit. Check these names/settings in
# the the "GEANT4_ENVIRONMENT" cmake variable inside cmake/FindGeant4.cmake
set( ATLAS_GEANT4_REPOSITORY
   "https://gitlab.cern.ch/atlas-simulation-team/geant4.git"
   CACHE STRING "Git repository for Geant4" )
# Git tag to build:
set( ATLAS_GEANT4_TAG "v10.6.3.3"
   CACHE STRING "Git tag for the version of Geant4 to build" )
mark_as_advanced( ATLAS_GEANT4_REPOSITORY ATLAS_GEANT4_TAG )

# Version string to replace:
set( ATLAS_G4VERS_ORIG "geant4.10.6.patch03.atlas03"
   CACHE STRING "Version string found in the specified tag of Geant4" )
# Version string to use:
set( ATLAS_G4VERS_PATCH "geant4.10.6.patch03.atlasmt03"
   CACHE STRING "Version string to switch to in the Geant4 build" )
mark_as_advanced( ATLAS_G4VERS_ORIG ATLAS_G4VERS_PATCH )

# Set some environment variables for the build:
if( APPLE )
   set( ENV{G4SYSTEM} "Darwin-g++" )
else()
   set( ENV{G4SYSTEM} "Linux-g++" )
endif()
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
   set( ENV{G4DEBUG} "1" )
endif()
set( ENV{G4LIB_BUILD_GDML} "1" )
set( ENV{G4LIB_BUILD_SHARED} "1" )
set( ENV{G4VIS_BUILD_DAWN_DRIVER} "1" )
set( ENV{G4VIS_BUILD_DAWNFILE_DRIVER} "1" )
set( ENV{G4VIS_BUILD_VRML_DRIVER} "1" )
set( ENV{G4VIS_BUILD_VRMLFILE_DRIVER} "1" )
set( ENV{G4INSTALL} "${CMAKE_CURRENT_BINARY_DIR}/build" )

# Extra options for the configuration:
set( _extraOptions )
set( _cmakePrefixes )

if( "${CMAKE_CXX_STANDARD}" EQUAL 11 )
   list( APPEND _extraOptions -DGEANT4_BUILD_CXXSTD:STRING=c++11 )
elseif( "${CMAKE_CXX_STANDARD}" EQUAL 14 )
   list( APPEND _extraOptions -DGEANT4_BUILD_CXXSTD:STRING=c++14 )
elseif( "${CMAKE_CXX_STANDARD}" EQUAL 17 )
   list( APPEND _extraOptions -DGEANT4_BUILD_CXXSTD:STRING=c++17 )
endif()
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Expat from LCG
list( APPEND _cmakePrefixes "${EXPAT_LCGROOT}" )

# VecGeom from AtlasExternals
list( APPEND _cmakePrefixes "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}")

# Handle all the externals that need to be set up through CMAKE_PREFIX_PATH.
if( _cmakePrefixes )
   list( REMOVE_DUPLICATES _cmakePrefixes )
   list( APPEND _extraOptions 
       -DCMAKE_PREFIX_PATH:PATH=${_cmakePrefixes} )
endif()

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/Geant4Build" )
# Directory holding the "stamp" files.
set( _stampDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/Geant4Stamp" )

# Create the script that will sanitize the geant4-config script after the build:
configure_file( "${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeConfig.sh.in"
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh"
   @ONLY )

# Build Geant4 for the build area:
ExternalProject_Add( Geant4
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   STAMP_DIR "${_stampDir}"
   GIT_REPOSITORY "${ATLAS_GEANT4_REPOSITORY}"
   GIT_TAG "${ATLAS_GEANT4_TAG}"
   PATCH_COMMAND sed -i "s/${ATLAS_G4VERS_ORIG}/${ATLAS_G4VERS_PATCH}/g"
   "<SOURCE_DIR>/source/global/management/include/G4Version.hh"
   COMMAND sed -i "s/${ATLAS_G4VERS_ORIG}/${ATLAS_G4VERS_PATCH}/g"
   "<SOURCE_DIR>/source/run/src/G4RunManagerKernel.cc"
   CMAKE_CACHE_ARGS
   -DGEANT4_USE_GDML:BOOL=ON
   -DGEANT4_USE_SYSTEM_ZLIB:BOOL=OFF
   -DCLHEP_ROOT_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   -DXERCESC_ROOT_DIR:PATH=${XERCESC_LCGROOT}
   -DGEANT4_USE_SYSTEM_CLHEP:BOOL=ON
   -DGEANT4_USE_SYSTEM_CLHEP_GRANULAR:BOOL=OFF
   -DGEANT4_BUILD_MULTITHREADED:BOOL=ON
   -DGEANT4_BUILD_TLS_MODEL:STRING=global-dynamic
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_LIBDIR:STRING=lib
   -DGEANT4_INSTALL_DATA:BOOL=OFF
   -DGEANT4_USE_USOLIDS:STRING=CONS;POLYCONE
   ${_extraOptions}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( Geant4 cleansource
   COMMAND ${CMAKE_COMMAND} -E remove -f
   "${_stampDir}/Geant4-gitclone-lastrun.txt"
   DEPENDERS download )
# Need to modify the printout here when the Geant4 version is updated
ExternalProject_Add_Step( Geant4 forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of Geant4 (2021.10.19.)"
   DEPENDERS cleansource )
ExternalProject_Add_Step( Geant4 purgebuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Geant4"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( Geant4 forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the configuration of Geant4"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( Geant4 buildinstall
   COMMAND "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}/lib/Geant4-10.6.3"
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing Geant4 into the build area"
   DEPENDEES install )
add_dependencies( Package_Geant4 Geant4 )
add_dependencies( Geant4 CLHEP )
add_dependencies( Geant4 VecGeom )

# Install Geant4:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES "cmake/FindGeant4.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
