COOL - The Conditions Database Project of LCG AA
================================================

This package builds COOL for ATLAS offline software projects that do not depend
on tdaq-common, but do depend on COOL.
