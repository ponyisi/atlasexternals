# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building/configuring prmon as part of the offline
# software build.
#

# Set the name of the package:
atlas_subdir( prmon )

# Set the source
set( ATLAS_PRMON_SOURCE
   "https://atlas-software-dist-eos.web.cern.ch/externals/prmon/v3.0.1.tar.gz"
   CACHE STRING "prmon source file to use" )
set( ATLAS_PRMON_HASH "3fdfcf4ba9bbbaf078b364ed6a7160cf"
   CACHE STRING "MD5 hash for the prmon source file" )
mark_as_advanced( ATLAS_PRMON_SOURCE ATLAS_PRMON_HASH )

# Print what's going on
message( STATUS "Buliding prmon as part of this project" )

# External dependencies
if( NOT ATLAS_BUILD_NLOHMANN_JSON )
   find_package( nlohmann_json )
endif()

# Temporary directory for the build results
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/prmonBuild" )

# List of paths given to CMAKE_PREFIX_PATH.
set( _prefixPaths "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   $ENV{CMAKE_PREFIX_PATH} ${nlohmann_json_DIR} )

# Extras
set( _extraArgs )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()
if( NOT "${CMAKE_CXX_STANDARD}" STREQUAL "" )
   list( APPEND _extraArgs -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Build prmon for the build area:
ExternalProject_Add( prmon
  PREFIX "${CMAKE_BINARY_DIR}"
  INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
  URL "${ATLAS_PRMON_SOURCE}"
  URL_MD5 "${ATLAS_PRMON_HASH}"
  CMAKE_CACHE_ARGS
  -DCMAKE_PREFIX_PATH:PATH=${_prefixPaths}
  -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
  -DCMAKE_INSTALL_BINDIR:PATH=${CMAKE_INSTALL_BINDIR}
  -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   ${_extraArgs}
  LOG_CONFIGURE 1 )
ExternalProject_Add_Step( prmon buildinstall
  COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
  COMMENT "Installing prmon into the build area"
  DEPENDEES install )
add_dependencies( prmon nlohmann_json )
add_dependencies( Package_prmon prmon )

# Install it here
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
