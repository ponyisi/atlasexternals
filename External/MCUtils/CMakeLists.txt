# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building MCUtils as part of the offline/analysis software.
#

# Declare the name of the package:
atlas_subdir( MCUtils )

# The source code of MCUtils:
set( ATLAS_MCUTILS_SOURCE
   "http://cern.ch/lcgpackages/tarFiles/sources/MCGeneratorsTarFiles/mcutils-1.3.5.tar.gz"
   CACHE STRING "MCUtils source file to use" )
set( ATLAS_MCUTILS_HASH "66a0ce4f746d65b1d9ab12373de659ee"
   CACHE STRING "MD5 hash for the MCUtils source file" )
mark_as_advanced( ATLAS_MCUTILS_SOURCE ATLAS_MCUTILS_HASH )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/MCUtilsBuild" )

# "Build" MCUtils:
ExternalProject_Add( MCUtils
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   URL "${ATLAS_MCUTILS_SOURCE}"
   URL_MD5 "${ATLAS_MCUTILS_HASH}"
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo
   "Configuring the installation of MCUtils"
   BUILD_COMMAND make install PREFIX=${_buildDir}
   INSTALL_COMMAND make install PREFIX=<INSTALL_DIR> )
add_dependencies( Package_MCUtils MCUtils )

# Install MCUtils:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
