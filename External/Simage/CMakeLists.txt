# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# CMake configuration file describing the build and installation of the
# Coin3D-simage library.
#

# The name of the package:
atlas_subdir( Simage )

# Directory for the temporary build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/SimageBuild" )

# The sources:
set( ATLAS_SIMAGE_SOURCE
   "http://cern.ch/atlas-software-dist-eos/externals/Simage/Coin3D-simage-2c958a61ea8b.zip"
   CACHE STRING "Simage source file to use" )
set( ATLAS_SIMAGE_HASH "7440b33776519ce19f536018a2adea3f"
   CACHE STRING "MD5 hash for the Simage source file" )
mark_as_advanced( ATLAS_SIMAGE_SOURCE ATLAS_SIMAGE_HASH )

# Figure out the build platform to specify for Simage. This is necessary because
# the Simage source code can not recognise some of the platforms that we now
# use. (Most notably aarch64.) Even though the Simage build doesn't seem to use
# this platform name in any useful name, it still fails if it can't figure out
# what platform it's running on. :-/
string( TOLOWER
   "${CMAKE_HOST_SYSTEM_PROCESSOR}-unknown-${CMAKE_HOST_SYSTEM_NAME}"
   _buildArch )

# Build the library for the build area:
ExternalProject_Add( Simage
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   URL "${ATLAS_SIMAGE_SOURCE}"
   URL_MD5 "${ATLAS_SIMAGE_HASH}"
   CONFIGURE_COMMAND <SOURCE_DIR>/configure --prefix=${_buildDir}
   --build=${_buildArch}
   BUILD_IN_SOURCE 1
   PATCH_COMMAND patch -p1 <
   "${CMAKE_CURRENT_SOURCE_DIR}/patches/libpng_centos7.patch"
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>" )
ExternalProject_Add_Step( Simage forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "Forcing the re-download of Simage"
   DEPENDERS download )
add_dependencies( Package_Simage Simage )

# Install the package:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
