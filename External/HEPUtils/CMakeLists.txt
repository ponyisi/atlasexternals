# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building HEPUtils as part of the offline/analysis software.
#

# Declare the name of the package:
atlas_subdir( HEPUtils )

# The source code of HEPUtils:
set( ATLAS_HEPUTILS_SOURCE
   "http://cern.ch/lcgpackages/tarFiles/sources/MCGeneratorsTarFiles/heputils-1.3.2.tar.gz"
   CACHE STRING "HEPUtils source file to use" )
set( ATLAS_HEPUTILS_HASH "b001e9462f3a575c0c585a8445d71914"
   CACHE STRING "MD5 hash for the HEPUtils source file" )
mark_as_advanced( ATLAS_HEPUTILS_SOURCE ATLAS_HEPUTILS_HASH )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/HEPUtilsBuild" )

# "Build" HEPUtils:
ExternalProject_Add( HEPUtils
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   URL "${ATLAS_HEPUTILS_SOURCE}"
   URL_MD5 "${ATLAS_HEPUTILS_HASH}"
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo
   "Configuring the installation of HEPUtils"
   BUILD_COMMAND make install PREFIX=${_buildDir}
   INSTALL_COMMAND make install PREFIX=<INSTALL_DIR> )
add_dependencies( Package_HEPUtils HEPUtils )

# Install MCUtils:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
