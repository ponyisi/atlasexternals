# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building Eigen as part of the offline / analysis software build.
#

# The name of the package:
atlas_subdir( Eigen )

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_EIGEN )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Eigen as part of this project" )

# The source of eigen:
set( ATLAS_EIGEN_SOURCE
   "http://cern.ch/lcgpackages/tarFiles/sources/eigen-3.3.7.tar.gz"
   CACHE STRING "Eigen source file to use" )
set( ATLAS_EIGEN_HASH "136a4ae08b56eda4eee0fbe62a27dcf9"
   CACHE STRING "MD5 has for the Eigen source file" )
mark_as_advanced( ATLAS_EIGEN_SOURCE ATLAS_EIGEN_HASH )

# Temporary directory for the build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/EigenBuild" )

# Extra options for the configuration:
set( _extraOptions )
if( "${CMAKE_CXX_STANDARD}" EQUAL 11 )
   list( APPEND _extraOptions -DEIGEN_BUILD_CXXSTD:STRING=-std=c++11 )
elseif( "${CMAKE_CXX_STANDARD}" EQUAL 14 )
   list( APPEND _extraOptions -DEIGEN_BUILD_CXXSTD:STRING=-std=c++14 )
elseif( "${CMAKE_CXX_STANDARD}" GREATER_EQUAL 17 )
   list( APPEND _extraOptions -DEIGEN_BUILD_CXXSTD:STRING=-std=c++17 )
endif()

# Build Eigen for the build area:
ExternalProject_Add( Eigen
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   URL "${ATLAS_EIGEN_SOURCE}"
   URL_MD5 "${ATLAS_EIGEN_HASH}"
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   ${_extraOptions}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( Eigen purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for Eigen"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( Eigen buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing Eigen into the build area"
   DEPENDEES install )
add_dependencies( Package_Eigen Eigen )

# And now install it:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
