AtlasLCG Test Project
=====================

This small test project is here to allow us to test all of the find-modules
coming with AtlasLCG against a selectable LCG release, with a selectable
LCG platform.

To use it, execute something like:

```
cmake -DLCG_VERSION_NUMBER=94 -DLCG_VERSION_POSTFIX=a \
   -DLCG_PLATFORM=x86_64-centos7-gcc8-opt ../atlasexternals/Build/AtlasLCG/test/
```

And then study the printed messages and the generated `env_setup.sh` file to
determine whether the code appears to be working correctly...
