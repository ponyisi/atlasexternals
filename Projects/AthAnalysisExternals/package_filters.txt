# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthAnalysisExternals.
#
+ External/BAT
+ External/COOL
+ External/CORAL
+ External/flake8_atlas
+ External/Gaudi
+ External/GPerfTools
+ External/GoogleTest
+ External/HDF5
+ External/KLFitter
+ External/Lhapdf
+ External/lwtnn
+ External/onnxruntime
+ External/PyModules
+ External/CLHEP
+ External/nlohmann_json
+ External/yampl
- .*
